/** Batch qui calcule les points de participation � des �v�nements commerciaux des comptes **/
global class BatchEventsAccountPointsCalculation implements Database.Batchable<sObject>, Database.Stateful{

	private String query;
	private Integer currentYear;
	private List<Account> accountList;
	private String silverLabel = System.Label.Label_silver;
	private String goldLabel = System.Label.Label_gold;
	private String platiniumLabel = System.Label.Label_platinium;

	global Database.QueryLocator start(Database.BatchableContext bc){
		
		this.accountList = new List<Account>();
		
		this.currentYear = Date.today().year();
		
		this.query = 'SELECT Id, Name, Level__c, Points__c, (SELECT Id, Account__c, Events_Business_Points__r.Silver_level_points__c, Events_Business_Points__r.Gold_level_points__c, Events_Business_Points__r.Platinium_level_points__c, Events_Business_Points__r.Year__c FROM Participations__r WHERE Events_Business_Points__r.Year__c = :currentYear) FROM Account WHERE Level__c != NULL';
		
		return Database.getQueryLocator(this.query);
	}
	
	global void execute(Database.BatchableContext BC, List<sObject> scope){
		
		// Pour chaque compte
		for(sObject aScope : scope){
			
			Account anAccount = (Account) aScope;
			
			Decimal nbPoints = 0;
			
			// Pour chaque participation du compte
			for(Participation__c aParticipation : anAccount.Participations__r){
				
				// Si compte silver
				if(anAccount.Level__c.equals(silverLabel) && aParticipation.Events_Business_Points__r.Silver_level_points__c != null){
					nbPoints += aParticipation.Events_Business_Points__r.Silver_level_points__c;
				}
				// Sinon si compte gold
				else if(anAccount.Level__c.equals(goldLabel) && aParticipation.Events_Business_Points__r.Gold_level_points__c != null){
					nbPoints += aParticipation.Events_Business_Points__r.Gold_level_points__c;
				}
				// Sinon si compte platinium
				else if(anAccount.Level__c.equals(platiniumLabel) && aParticipation.Events_Business_Points__r.Platinium_level_points__c != null){
					nbPoints += aParticipation.Events_Business_Points__r.Platinium_level_points__c;
				}
			}
			
			anAccount.Points__c = nbPoints;
			
			this.accountList.add(anAccount);
		}
		
		// Mise a jour des comptes
		update this.accountList;
	}
	
	global void finish(Database.BatchableContext BC){
		
	}
}