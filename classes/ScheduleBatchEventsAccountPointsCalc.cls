global class ScheduleBatchEventsAccountPointsCalc implements Schedulable{

	global void execute(SchedulableContext ctx) {
        
        Database.executeBatch(new BatchEventsAccountPointsCalculation());
    
    }
}