/**  **/
@isTest
private class BatchEventsAccountPointsCalculation_TEST {

	static List<Account> accountList;
	static List<Business_Event__c> businessEventList;
	static List<Events_Business_Points__c> eventBusinessPointList;
	static List<Participation__c> participationList;
	static String silverLabel = System.Label.Label_silver;
	static String goldLabel = System.Label.Label_gold;
	static String platiniumLabel = System.Label.Label_platinium;
	static Date todayDate;

	static void init(){
		todayDate = Date.today();
		
		/** Account **/
			accountList = new List<Account>();
			accountList.add(new Account(
				Name = 'Test name silver',
				Level__c = silverLabel
			));
			accountList.add(new Account(
				Name = 'Test name gold',
				Level__c = goldLabel
			));
			accountList.add(new Account(
				Name = 'Test name platinium',
				Level__c = platiniumLabel
			));
			
			insert accountList;
		
		/** Business_Event__c **/
			businessEventList = new List<Business_Event__c>();
			businessEventList.add(new Business_Event__c(
				Name = 'Salesforce1 World Tour Paris',
				Place__c = 'Paris'
			));
			businessEventList.add(new Business_Event__c(
				Name = 'Dreamforce',
				Place__c = 'Los Angeles'
			));
			businessEventList.add(new Business_Event__c(
				Name = 'Salesforce1 World Tour London',
				Place__c = 'London'
			));
			
			insert businessEventList;
		
		/** Events_Business_Points__c **/
			eventBusinessPointList = new List<Events_Business_Points__c>();
			eventBusinessPointList.add(new Events_Business_Points__c(
				Business_Event__c = businessEventList[0].Id,
				Year__c = todayDate.year()-1,
				Silver_level_points__c = 10,
				Gold_level_points__c = 15,
				Platinium_level_points__c = 30
			));
			eventBusinessPointList.add(new Events_Business_Points__c(
				Business_Event__c = businessEventList[0].Id,
				Year__c = todayDate.year(),
				Silver_level_points__c = 20,
				Gold_level_points__c = 35,
				Platinium_level_points__c = 40
			));
			eventBusinessPointList.add(new Events_Business_Points__c(
				Business_Event__c = businessEventList[1].Id,
				Year__c = todayDate.year()-1,
				Silver_level_points__c = 100,
				Gold_level_points__c = 150,
				Platinium_level_points__c = 300
			));
			eventBusinessPointList.add(new Events_Business_Points__c(
				Business_Event__c = businessEventList[1].Id,
				Year__c = todayDate.year(),
				Silver_level_points__c = 150,
				Gold_level_points__c = 300,
				Platinium_level_points__c = 450
			));
			eventBusinessPointList.add(new Events_Business_Points__c(
				Business_Event__c = businessEventList[2].Id,
				Year__c = todayDate.year(),
				Silver_level_points__c = 120,
				Gold_level_points__c = 270,
				Platinium_level_points__c = 430
			));
			
			insert eventBusinessPointList;
		
		/** Participation__c **/
			participationList = new List<Participation__c>();
			participationList.add(new Participation__c(
				Account__c = accountList[0].Id,
				Events_Business_Points__c = eventBusinessPointList[0].Id
			));
			participationList.add(new Participation__c(
				Account__c = accountList[0].Id,
				Events_Business_Points__c = eventBusinessPointList[1].Id
			));
			participationList.add(new Participation__c(
				Account__c = accountList[1].Id,
				Events_Business_Points__c = eventBusinessPointList[4].Id
			));
			participationList.add(new Participation__c(
				Account__c = accountList[1].Id,
				Events_Business_Points__c = eventBusinessPointList[0].Id
			));
			participationList.add(new Participation__c(
				Account__c = accountList[2].Id,
				Events_Business_Points__c = eventBusinessPointList[1].Id
			));
			participationList.add(new Participation__c(
				Account__c = accountList[2].Id,
				Events_Business_Points__c = eventBusinessPointList[2].Id
			));
			participationList.add(new Participation__c(
				Account__c = accountList[2].Id,
				Events_Business_Points__c = eventBusinessPointList[4].Id
			));
			
			insert participationList;
	}

	/** Test les points du compte **/
    static testMethod void testAccountPoint() {
        
        init();
        
        Test.startTest();
        
        Database.executeBatch(new BatchEventsAccountPointsCalculation());
        
        Test.stopTest();
        
        List<Account> newAccountList = [
        	SELECT Id, Name, Points__c
        	FROM Account
        	WHERE Id IN :accountList
        ];
        
        // Verification
        System.assertEquals(eventBusinessPointList[1].Silver_level_points__c, newAccountList[0].Points__c);
        System.assertEquals(eventBusinessPointList[4].Gold_level_points__c, newAccountList[1].Points__c);
        System.assertEquals((eventBusinessPointList[1].Platinium_level_points__c + eventBusinessPointList[4].Platinium_level_points__c), newAccountList[2].Points__c);
    }
}